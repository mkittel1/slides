## DDB Workshop
## OpenRefine mit GREL und Python

Maike Kittelmann

<small>kittelmann@sub.uni-goettingen.de</small>

<small>Goettingen State and University Library</small>

---

<!-- ####################################################### -->
<!-- ####################################################### -->

![Logo](https://upload.wikimedia.org/wikipedia/commons/4/4b/OpenRefine_New_Logo.png)

[http://openrefine.org/](http://openrefine.org/)


--

* seit 2010
* anfangs Google-Produkt ("GoogleRefine")
* Open Source ("OpenRefine")
* JAVA
* Extensions

--

→ Kurze Tour der Oberfläche und Funktionen








<!-- ####################################################### -->
<!-- ####################################################### -->

---

## Python Intro für OpenRefine

--

![Jython](https://upload.wikimedia.org/wikipedia/en/e/ea/Jython.png)

--

Text ausgeben

```
return "test"
```

--

Rechnen

```
return 1 + 1
```


\+ \- \* / %

--

Kommentare 

```
# alles hinter der Raute wird nicht ausgegeben
```

--

```
return "test"   # dies ist ein Kommentar
```

```
return 1 + 1    # und dies auch
```

--

⚠️ 

Datentyp String ≠ Datentyp Integer

Zeichenkette ≠ Ganzzahl

"1" ≠ 1

--

Übung: Was passiert?

```
return 1 + 1
```

```
return '1' + '1'
```

```
return "1" + "1"
```

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Was passiert?

```
return 1 + 1         # 2
```

```
return '1' + '1'     # '11'
```

```
return "1" + "1"     # "11"
```

--


Übung: Gibt es eine Fehlermeldung?

```
return 1 + 1
```

```
return '1' + 1
```

```
return "1" + '1'
```

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Gibt es eine Fehlermeldung?

```
return 1 + 1      # rechnet 
```

```
return '1' + 1    # ⛔
```

```
return "1" + '1'  # konkateniert
```


--

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

Variablen

```
a = "test"
return a
```

```
b = 1
return b
```

--

Variablennamen

| Erlaubte  | Zeichen |  |
|----------|--------|--------|
| A-Z a-z  | Buchstabe  |  |
| 0-9      | Ziffer | aber nicht als erstes Zeichen |
| _        | Unterstrich  |  |

--

camelCase 🐫 vs. snake_case 🐍 

--

KONSTANTE

```
GLEICHBLEIBEND = ...
```

--
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Datentypen (kleine Auswahl)

--
 
String

```
"dies ist ein String" 
'und dies auch'
```

--

Integer = Ganzzahl

```
666
```

--

Fließkommazahl

```
1.1     ⚠️ Punkt als Trenner!
```

--
Boole'scher Wert

```
True
False
```

--

Gar kein Wert

```
None
```

--

### Sequenzielle Datentypen

--

Listen

```
obst = ["Apfel", "Banane", "Kirsche"]
```

--

Auf Werte in Listen zugreifen

```
obst = ["Apfel", "Banane", "Kirsche"]
return obst[0]   # ⚠️ Listen haben einen 0-basierten Index!
```

--

Auf Werte in Listen zugreifen

```
obst = ["Apfel", "Banane", "Kirsche"]
return obst[0:2]   # ⚠️ Listen haben einen 0-basierten Index!
```


--

| Syntax   | bewirkt
|----------|---------------------------------------| 
| a[i]     | Gibt i'tes Element von Liste a zurück | 
| a[i:j]   | Gibt den Bereich von Index i bis Index j OHNE j zurück | 
| a[i:j:k] | Gibt den Bereich a[i:j] in Schritten von k zurück| 


--

Werte in Listen verändern

```
obst = ["Apfel", "Banane", "Kirsche"]
obst[1] = "Johannisbeere"
```

--

Datentyp herausfinden

```
return type(x)
```

--

Übung: Datentypen

```
obst = ["Apfel", "Banane", "Kirsche"]
```

1. Wie greift man auf den Listenwert "Kirsche" zu?

2. Wie tauscht man "Apfel" gegen "Pflaume" aus?

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Datentypen 

```
obst = ["Apfel", "Banane", "Kirsche"]
```

Wie greift man auf den Listenwert "Kirsche" zu?

```
return obst[2]
```

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Datentypen 

```
obst = ["Apfel", "Banane", "Kirsche"]
```

Wie tauscht man den Wert "Apfel" gegen "Pflaume" aus?

```
obst[0] = "Pflaume"
```


<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

--

Übung: Datentypen ff


Was für eine Ausgabe erzeugt dieser Code? 

```
list = ["Apfel", 1, True]
return type( list[1] )
```

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Datentypen ff


Was für eine Ausgabe erzeugt dieser Code?

```
list = ["Apfel", 1, True]
return type( list[1] )      # <type 'bool'>
```


```
list = ["Apfel", 1, True]   # in Python (und einigen anderen Sprachen) erlaubt, aber nicht in allen Programmiersprachen
```

--

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

Typecast: Integer zu String

```
return str( 1 )
```

```
return str( 1 ) + "1"
```

--

Typecast: String zu Integer

```
return int( "1" )
```

```
return int( "1" ) + 1
```


--

<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Kontrollstrukturen

--

WENN - DANN

```
itRains = True
if itRains:
   return "Regenschirm nicht vergessen!"
```

--
WENN - DANN - ANDERNFALLS

```
itRains = False
if itRains:
   return "Regenschirm nicht vergessen!"
else:
   return "Glück gehabt!" 
```

--
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Übung: Indentationsfehler

[Indentation](https://www.w3schools.com/python/showpython.asp?filename=demo_indentation_test)

--

<!-- .slide: data-background-color="yellow" -->

## Lösung: Indentationsfehler

```
if 5 > 2:
   print("Five is greater than two!")
```

--
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Schleifen / Loops

--

for

```
fruits = ["Apfel", "Banane", "Kirsche"]
for x in fruits:
    print(x)
```

--
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Rückgabewert

```
fruits = ["Apfel", "Banane", "Kirsche"]
for x in fruits:
  return x
```

--
<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->

## Funktionen

--

Eigene Funktionen definieren

```
def meine_Funktion():
    return "Meine erste Funktion!"
```

--

Eigene Funktionen definieren

```
def noch_eine_Funktion():
   return 1 + 1
```

--

* Eine eigene Funktionsdefinition beginnt mit "def"
* es folgt der Funktionsname plus "()"
* die erste Zeile endet mit ":" 
* Der "Körper" der Definition muss eingerückt werden!
* Eine Funktion produziert nur Output, wenn sie einen RÜCKGABEWERT hat.

--

Eine Funktion aufrufen

```
def meine_Funktion():
   return "Meine erste Funktion!"

return meine_Funktion()
```

--

Funktionen mit Parametern

```
def meine_Funktion( name ):          # ⚠️ auf Datentypen achten!
    return name + "s Funktion!"  
```

```
def noch_eine_Funktion( n ):         # ⚠️ auf Datentypen achten!
   return 1 + n
```

```
def noch_eine_Funktion( n ):         # ⚠️ auf Datentypen achten!
   if n:
      return "Yeah!"
```

--

## Funktion vs. Methode

Funktion  

```
meine_funktion()
```

Methode

```
mein_objekt.meine_methode()
```

--

## Objekt

Datenstruktur, die 
* Eigenschaften (properties) und 
* Verhalten (methods)
bereits mitbringt. 

```
mein_objekt.meine_eigenschaft
```

```
mein_objekt.meine_methode()
```

--

Funktionsbibliotheken importieren

```
import re
```


```
from math import pi
```

--

Funktionsbibliotheken verwenden

import ...

```
import re
output = "Donald Trump" 
result = re.sub( 'Trump', 'Duck', output )
return result
```

from ... import ...

```
from math import pi
return pi
```

--

# Ressourcen

[Python Dokumentation](https://docs.python.org/3/)

[Python Tutorial](https://docs.python.org/3/tutorial/)

[W3C Python Tutorial](https://www.w3schools.com/python/python_intro.asp)










---

<!-- ####################################################### -->
<!-- ####################################################### -->

## Programmieren in OpenRefine - Grundsätzliches

--

### Wo?

Edit column > Add column based on this column


--
### Einfachen Text ausgeben


GREL
```
"test"
```

Python
```
return "test"
```


--

Wo sind meine Daten?


![Paket](https://media.diepresse.com/images/uploads_620/7/0/5/5367557/726D5C9E-D85C-4BEA-9EB2-E0A2607774CC_v0_h.jpg)

--

value 📦

row 📦

cells 📦

cell 📦 

record 📦 

recon 📦


--

value 📦

= der Wert im aktuellen Feld 

GREL
```
value
```

Python
```
return value
```

--

cell 📦

= das aktuelle Feld


GREL
```
cell.value
```

Python
```
return cell.value
```

--

cells 📦

= alle Felder einer Zeile


GREL
```
cells.[ÜBERSCHRIFT DER SPALTE].value
```

Python
```
return cells.[ÜBERSCHRIFT DER SPALTE].value
```

--

row 📦

= die aktuelle Zeile

GREL
```
row.index
```

Python
```
return row.index
```


--

row 📦

= die aktuelle Zeile

GREL
```
row.columnNames
```

Python
```
return row.columnNames
```

--

row 📦

= die aktuelle Zeile

<small>

| Attribut | Beschreibung |
|----------|--------|
| row.index | 	zero-based index of the current row |
| row.cells |	the cells of the row, same as the "cells" variable above |
| row.columnNames |	the column names of the row (i.e. the column names in the project) |
| row.starred |	boolean, indicating if the row is starred |
| row.flagged |	boolean, indicating if the row is flagged |
| row.record |	the Record object containing the current row, same as the record variable above |

</small>

--

record 📦 

= alle Felder eines Records


GREL
```
row.record.index
```

Python
```
return row.record.index
```

--

record 📦 

= alle Felder eines Records


GREL
```
row.record.index
```

Python
```
return row.record.index 
```


--

record 📦 

= alle Felder eines Records


GREL
```
row.record.fromRowIndex
```

Python
```
return row.record.fromRowIndex 
```

--

record 📦 

= alle Felder eines Records

<small>

| Attribut | Beschreibung | Beispiel |
|----------|--------|--------|
| row.record.index | 	zero-based index of the current record | 	evaluating row.record.index on row 2 returns 0 | 
| row.record.cells | 	the cells of the row |	evaluating row.record.cells.book.value on row 2 returns [ "Anathem", "Snow Crash" ] | 
| row.record.fromRowIndex |	zero based index of the first row in the record |	evaluating row.record.fromRowIndex on row 2 returns 0 | 
| row.record.toRowIndex | index (not zero based) of the last row in the record |	evaluating row.record.toRowIndex on row 2 returns 2 | 

</small>

--

#### Zusammenfassung

<small>

| 📦 | Bedeutung | 
|----|----------| 
| value | the value of the cell in the base column of the current row; can be null |
| row   | the current row; an object with more fields |
| cells | the cells of the current row, with fields that correspond to the column names |
| cell  | the cell in the base column of the current row; an object with more fields |
| recon | the recon object of a cell returned from a reconciliation service or provider; an object with more fields |
| record| one or more rows grouped together to form a record; an object with more fields | 

</small>




<!-- ####################################################### -->
<!-- ####################################################### -->

---


## GREL und Python - Gemeinsamkeiten und Unterschiede

--

GREL
```
'Record beginnt in Zeile ' + row.record.fromRowIndex
```

Python
```
return 'Record beginnt in Zeile ' + str( row.record.fromRowIndex )
```



--

### Ersetzen des ersten Zeichens eines Strings

--

## GREL

```
value.replace( /^./, '' )
```

## Python

```
import re
result = re.sub( '^.', '', value )
return result
```

--

Alle Spaltenüberschriften

GREL

```
row.columnNames
```

Python

```
return row.columnNames
```

--

Der Werte einer **bestimmten** Spalte der aktuellen Zeile

GREL

```
row.cells.spatial.value
```

```
cells.spatial.value
```


Python

```
return row.cells.spatial.value
```

```
return cells.spatial.value
```


--

Die Werte aller Spalten der aktuellen Zeile

Python

```
my_list = []
for name in row.columnNames:
    a = 'row.cells.' + name + '.value' 
    my_list.insert( -1, eval(a) )
my_list_str = [str(i) for i in my_list if i] 
return ",".join( my_list_str )
```

--

Die Werte aller Spalten der aktuellen Zeile in umgekehrter Reihenfolge

```
my_list = []
for name in row.columnNames:
    a = 'row.cells.' + name + '.value' 
    my_list.insert( 0, eval(a) )
my_list_str = [str(i) for i in my_list if i] 
return ",".join( my_list_str )
```
















--

GREL

```
row.record.cells.types.value
```

Python

```
return row.record.cells.types.value
```

--

Datentyp herausfinden

GREL

```
type( row.cells.test.value )
```

```
return type( row.cells.test.value )
```

--

Alle Spaltenüberschriften einer Zeile

```
return row.columnNames
```

ist nicht dasgleiche wie:

```
for name in row.columnNames:
   return name
```

--

Die Werte aller Spalten einer Zeile in umgekehrter Reihenfolge

```
my_list = []
for name in row.columnNames:
    a = 'row.cells.' + name + '.value' 
    my_list.insert( 0, eval(a) )
return my_list
```

--

#### None-Check

Python

```
  if value is not None:
    return value.lower()
  else:
    return None
```

--

##### Checks

GREL

```
isBlank, isNonBlank, isNull, isNotNull, isNumeric, isError
```

--

## WENN - DANN

GREL 
```
if("Deutsche Digitale Bibliothek".length() > 10, "big string", "small string")
```

Python
```
if len( "Deutsche Digitale Bibliothek" ) > 10:
     return "big string"
else:
     return "small string"     

```

--

## Iteration

= alle Werte einer Liste der Reihe nach bearbeiten

GREL

```
forEach(row.columnNames, v, v.toLowercase() )
```

Python

```
result = []
for v in row.columnNames:
    result.insert( -1, v )
return result
```

--

## Iteration mit Index

GREL

```
forEachIndex(expression a, variable i, variable v, expression e)
```

Python

```
result = []
for ix, v in enumerate( row.columnNames ):
    result.insert( -1, str(ix) + ': ' + v )
return result
```

--











```
with(expression o, variable v, expression e)
```

--

```
filter(expression a, variable v, expression test)
```

--

```
forRange(number from, number to, number step, variable v, expression e)
```

```
forNonBlank(expression o, variable v, expression eNonBlank, expression eBlank)
```

forRange(number from, number to, number step, variable v, expression e)

Iterates over the variable v starting at from, incrementing by step each time while less than to. At each iteration, evaluates expression e, and pushes the result onto the result array.
forNonBlank(expression o, variable v, expression eNonBlank, expression eBlank)


https://github.com/OpenRefine/OpenRefine/issues/200


--

https://github.com/OpenRefine/OpenRefine/wiki/Recipes

--


[Doku](https://github.com/OpenRefine/OpenRefine/wiki/GREL-Controls)








<!-- ####################################################### -->
<!-- ####################################################### -->

---

## Programmieren in OpenRefine - Übungen

--


Übung: Erzeugen diese beiden Python-Ausdrücke die gleiche Ausgabe?

```
return row.columnNames
```

```
for name in row.columnNames:
   return name
```

--

<!-- .slide: data-background-color="yellow" -->

Lösung: Erzeugen diese beiden Python-Ausdrücke die gleiche Ausgabe?

NEIN!!

```
return row.columnNames
```

```
for name in row.columnNames:
   return name
```

--




<!-- ####################################################### -->
<!-- ####################################################### -->

---

## GREL Intro

--

RTFM

[GREL Reference](https://github.com/OpenRefine/OpenRefine/wiki/General-Refine-Expression-Language)

--




<!-- ####################################################### -->
<!-- ####################################################### -->

--

# Reguläre Ausdrücke


```
import re
result = re.sub( '^.', '', value )
return result
```

--


<!-- ####################################################### -->
<!-- ####################################################### -->

return row.cells.yr.value


[Dokumentation](https://github.com/OpenRefine/OpenRefine/wiki/Variables)

---

return value[1:-1]

--
= row.cells.[Spaltenname].value


[Pip](https://github.com/OpenRefine/OpenRefine/wiki/Extending-Jython-with-pypi-modules)

--

# cells["types"]["value"]

return cells["mo"]["value"]

--

[Jython](https://github.com/OpenRefine/OpenRefine/wiki/Jython)

```
from com.google.i18n.phonenumbers import PhoneNumberUtil
from com.google.i18n.phonenumbers.PhoneNumberUtil import PhoneNumberFormat

phoneUtil = PhoneNumberUtil.getInstance()
number = phoneUtil.parse(value, 'US')
formatted = phoneUtil.format(number, PhoneNumberFormat.NATIONAL)
valid = phoneUtil.isValidNumber(number)

if valid == 1:
 return formatted
```




--

## DSL

Domain Specific Language






<!-- ####################################################### -->
<!-- ####################################################### -->



--

## Clojure

(.toString 2)

( str (.toString 2 ) "s" )


Add to array

( concat (.toString 2 ) "s" )



( str 2 "s" )



<!-- ####################################################### -->
<!-- ####################################################### -->


<!-- .slide: data-background-color="lightblue" -->

--

<!-- .slide: data-background-image="img/some-image.png" -->


--

